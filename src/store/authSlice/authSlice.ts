import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AuthApi } from 'api/auth';
import jwt_decode from 'jwt-decode';

export const login = createAsyncThunk(
  '/login',
  async (credential: any, { rejectWithValue }) => {
    // const{rejectWithValue} = thunkParams
    try {
      const response = await AuthApi.login(credential);

      return response;
    } catch (error: any) {
      if (!error.response) {
        throw error;
      }
      return rejectWithValue(error.response.data);
    }
  },
);

interface AuthState {
  isAuthenticated: boolean;
  isLogout: boolean;
  accessToken: any;
  refreshToken: any;
  item: any;
  mess: any;
}

const initialState: AuthState = {
  isAuthenticated: false,
  isLogout: false,
  accessToken: undefined,
  refreshToken: undefined,
  item: {},
  mess: undefined,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(login.fulfilled, (state, action: any) => {
      console.log('fulfilled state', action.payload.data.data);
      if (action.payload.data.data.access_token) {
        const { access_token } = action.payload.data.data;
        const { staffCode, role, email }: any = jwt_decode(access_token);

        state.isAuthenticated = true;
        state.isLogout = false;
        state.accessToken = access_token;
        state.item = {
          staffCode: staffCode,
          role: role,
          email: email,
        };
        console.log('action.payload.data.data', action.payload.data.data);

        localStorage.setItem(
          'staff_info',
          JSON.stringify(action.payload.data.data),
        );
        localStorage.setItem('token', state.accessToken);
      } else {
        state.item = {
          mess: action.payload.data.data,
        };
      }
    });
  },
});

const { reducer: authReducer } = authSlice;

export default authReducer;
