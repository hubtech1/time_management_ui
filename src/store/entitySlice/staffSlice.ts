import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { StaffApi } from 'api/entityStaff';

export const createStaff = createAsyncThunk(
  'staff/create',
  async (body: any, thunkParams) => {
    try {
      const response = StaffApi.create(body);

      return response;
    } catch (error) {
      throw error;
    }
  },
);

export const findAll = createAsyncThunk('staff/findall', async () => {
  try {
    const response = await StaffApi.findAll();

    return response;
  } catch (error) {
    throw error;
  }
});

export const findOne = createAsyncThunk(
  'staff/findone',
  async (id: string, thunkParams) => {
    try {
      const response = await StaffApi.findOne(id);

      return response;
    } catch (error) {
      throw error;
    }
  },
);

export const findColleague = createAsyncThunk(
  'staff/findcolleague',
  async (id: string, thunkParams) => {
    try {
      const response = await StaffApi.findOne(id);

      return response;
    } catch (error) {
      throw error;
    }
  },
);

export const initialState = {
  register: {
    staffCodeExist: undefined,
    // emailCodeExist: false,
  },
  list: {},
  staff: undefined,
  listColleague: [],
};

const staffSlice = createSlice({
  name: 'Staff',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(createStaff.fulfilled, (state: any, action: any) => {
        if (typeof action.payload.data !== 'string') {
          state.register.staffCodeExist = false;
          return;
        } else {
          state.register.staffCodeExist = true;
          return;
        }
      })
      .addCase(findAll.fulfilled, (state: any, action: any) => {
        state.list = action.payload.data.data;
      })
      .addCase(findOne.fulfilled, (state: any, action: any) => {
        state.staff = action.payload.data.data;
        if (action.payload.data.data.role === 0) {
          state.staff.role = 'Staff';
        } else if (action.payload.data.data.role === 1) {
          state.staff.role = 'Manager';
        } else if (action.payload.data.data.role === 2) {
          state.staff.role = 'Director';
        }
      })
      .addCase(findColleague.fulfilled, (state: any, action: any) => {
        state.listColleague.push(action.payload.data.data);
      });
  },
});

const { reducer: staffReducer } = staffSlice;

export default staffReducer;
