import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { DepartmentApi } from 'api/entityDepartment';

export const getDepartment = createAsyncThunk('department/get', async () => {
  try {
    const response = await DepartmentApi.find();

    return response;
  } catch (error) {
    throw error;
  }
});

const initialState = {
  list: [],
  findOne: undefined,
};

const departmentSlice = createSlice({
  name: 'department',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(getDepartment.fulfilled, (state, action: any) => {
      const departmentList = action.payload.data.data;
      state.list = departmentList;
      // console.log("departmentList",departmentList);
      // console.log("state",state.list);
    });
  },
});

const { reducer: departmentReducer } = departmentSlice;

export default departmentReducer;
