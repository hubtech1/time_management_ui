// 1. Setup todo slice

import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface actionDispatch {
  type: string;
  data: any;
}

const todoSlice = createSlice({
  name: 'todos',
  initialState: ['phần tử 1', 'phần tử 2', 'phần tử 3'],
  reducers: {
    addArray: (state, action: PayloadAction<any>) => {
      console.log('addArray state', state);
      console.log('addArray action', action);

      if (action.payload.trim() === '') {
        return alert('Cần nhập dữ liệu!!!');
      } else {
        state.push(action.payload);
      }
    },
    removeArray: (state, action: PayloadAction<any>) => {
      console.log(action.payload);

      state.splice(action.payload, 1);
    },
  },
});
const { actions, reducer } = todoSlice;

export const { addArray, removeArray } = actions;

export default reducer;
