import {
  AmazonOutlined,
  AntDesignOutlined,
  CalendarOutlined,
  CaretDownOutlined,
  InstagramOutlined,
} from '@ant-design/icons';
import { DownOutlined } from '@ant-design/icons';
import { Avatar, Dropdown, Image, Menu, Space } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { findOne } from 'store/entitySlice/staffSlice';
import './NavbarAvatar.css';

const menu = (
  <Menu
    items={[
      {
        label: <a href="/calendar">Calendar Component</a>,
        key: '0',
      },
      {
        label: <a href="/changepassword">Change Password</a>,
        key: '1',
      },
      {
        type: 'divider',
      },
      {
        label: <a href="/login">Log out</a>,
        key: '3',
      },
    ]}
  />
);

const staffInfo = {
  fullName: '',
  role: '',
  avatar: '',
};

const NavbarAvatar = (props: any) => {
  const localStorageInfo: any = localStorage.getItem('staff_info');
  const { staffCode } = JSON.parse(localStorageInfo);

  const { staff } = useSelector((state: any) => state.staffReducer);
  const dispatch = useDispatch();
  // console.log("NavbarAvatar", staff);

  if (staff) {
    staffInfo.fullName = staff.fullName;
    staffInfo.role = staff.role;
    staffInfo.avatar = staff.avatar;
  }

  // useEffect(() => {
  //   staffInfo.fullName = staff.fullName
  //   staffInfo.role = staff.role
  //   staffInfo.avatar = staff.avatar
  // },[staff])

  return (
    <div className="navbar_container">
      <div className="navbar_logo">
        <InstagramOutlined style={{ fontSize: '75px' }} />
      </div>

      <div className="navbar_content">
        <CalendarOutlined style={{ fontSize: '32px', color: '#ED2553' }} />
        <div className="navbar_content_body">Calendar</div>
      </div>

      <div className="navbar_avatar">
        <div className="navbar_avatar_img">
          <Avatar src={staffInfo.avatar} size="large" />
        </div>
        <div className="navbar_avatar_content">
          <div className="navbar_avatar_content_name">
            <Dropdown overlay={menu} trigger={['click']}>
              <a onClick={e => e.preventDefault()}>
                <Space direction="horizontal" align="center">
                  {staffInfo.fullName}
                  <CaretDownOutlined />
                </Space>
              </a>
            </Dropdown>
          </div>
          <div className="navbar_avatar_content_role">{staffInfo.role}</div>
        </div>
      </div>
    </div>
  );
};

export default NavbarAvatar;
