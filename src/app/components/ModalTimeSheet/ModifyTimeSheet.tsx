import React, { useState } from 'react';
import {
  Button,
  Form,
  Input,
  Col,
  Row,
  Upload,
  DatePicker,
  TimePicker,
  UploadProps,
} from 'antd';
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';
import styled from './NewTimeSheet.module.css';
import { UploadOutlined } from '@ant-design/icons';

const ModifyTimeSheet = () => {
  const [inputState, setInputState] = useState({
    title: '',
    description: '',
  });

  const timeSheetDetail = {
    staff_id: 'AW007',
    staff_name: 'Allen Walker',
    department: 'Dev',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Magni similique, corporis a sapiente asperiores ipsam cupiditate mollitia aperiam impedit vitae quod possimus fugiat aliqu',
    title: 'Convert UI Calendar',
    props: {
      action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
      onChange({ file, fileList }) {
        if (file.status !== 'uploading') {
          console.log(file, fileList);
        }
      },
      defaultFileList: [
        {
          uid: '1',
          name: 'xxx.png',
          status: 'done',
          response: 'Server Error 500', // custom error message to show
          url: 'http://www.baidu.com/xxx.png',
        },
        {
          uid: '2',
          name: 'yyy.png',
          status: 'done',
          url: 'http://www.baidu.com/yyy.png',
        },
      ],
    },
  };

  const { TextArea } = Input;

  const getDataReducer = () => {
    setInputState({
      title: timeSheetDetail.title,
      description: timeSheetDetail.description,
    });
    // console.log("123");
  };

  // console.log("inputState",inputState);

  const onChangeDate: DatePickerProps['onChange'] = (date, dateString) => {
    console.log(date, dateString);
  };

  const props: UploadProps = {
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange({ file, fileList }) {
      if (file.status !== 'uploading') {
        console.log(file, fileList);
      }
    },
    defaultFileList: [
      {
        uid: '1',
        name: 'xxx.png',
        status: 'done',
        response: 'Server Error 500', // custom error message to show
        url: 'http://www.baidu.com/xxx.png',
      },
      {
        uid: '2',
        name: 'yyy.png',
        status: 'done',
        url: 'http://www.baidu.com/yyy.png',
      },
    ],
  };

  const listTest: any = [
    {
      uid: '1',
      name: 'xxx.png',
      status: 'done',
      response: 'Server Error 500', // custom error message to show
      url: 'http://www.baidu.com/xxx.png',
    },
  ];

  return (
    <Row className={styled.auth_page}>
      <Col className={styled.auth_container} span={8}>
        <div className={styled.auth_form}>
          <div className={styled.auth_form_header}>
            <h1>Create Timesheet</h1>
            <h6>
              Staff code: <span>{timeSheetDetail.staff_id}</span>{' '}
            </h6>
            <h6>
              Staff name: <span>{timeSheetDetail.staff_name}</span>{' '}
            </h6>
            <h6>
              Staff department: <span>{timeSheetDetail.department}</span>
            </h6>
          </div>

          <Form
            className={styled.auth_form_content}
            name="basic"
            layout="vertical"
            autoComplete="off"
          >
            <Form.Item
              className={styled.auth_form_input}
              label="Title"
              name="title"
              rules={[{ required: true, message: 'Please input title' }]}
            >
              <Input value={inputState.title} />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Time"
              name="time"
              rules={[{ required: true, message: 'Please input Time!' }]}
            >
              <DatePicker onChange={onChangeDate} />
              <TimePicker.RangePicker use12Hours format="h:mm A" />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Description"
              // name="description"
              rules={[
                { required: true, message: 'Please input Task Description!' },
              ]}
            >
              <TextArea rows={4} value={inputState.description} />
            </Form.Item>

            <Upload defaultFileList={listTest}>
              <Button icon={<UploadOutlined />}>Click to Upload</Button>
            </Upload>

            <Row justify="center">
              <Col>
                <Button
                  className={styled.auth_form_button}
                  onClick={getDataReducer}
                  size="large"
                  shape="round"
                  type="primary"
                >
                  Nap du lieu
                </Button>
                <Button
                  className={styled.auth_form_button}
                  size="large"
                  shape="round"
                  type="primary"
                  htmlType="submit"
                >
                  {inputState.title || 'Not yet'}
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </Col>
    </Row>
  );
};

export default ModifyTimeSheet;
