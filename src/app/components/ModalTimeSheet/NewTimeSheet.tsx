import React, { useState } from 'react';
import {
  Button,
  Form,
  Input,
  Col,
  Row,
  Upload,
  DatePicker,
  TimePicker,
} from 'antd';
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';
import styled from './NewTimeSheet.module.css';
import { UploadOutlined } from '@ant-design/icons';

const timeSheetDetail = {
  staff_id: 'AW007',
  staff_name: 'Allen Walker',
  department: 'Dev',
  description:
    'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Magni similique, corporis a sapiente asperiores ipsam cupiditate mollitia aperiam impedit vitae quod possimus fugiat aliqu',
  title: 'Convert UI Calendar',
};

const { TextArea } = Input;

const NewTimeSheet = () => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const onChangeDate: DatePickerProps['onChange'] = (date, dateString) => {
    console.log(date, dateString);
  };

  return (
    <Row className={styled.auth_page}>
      {/* <Col span={8}></Col> */}
      <Col className={styled.auth_container} span={8}>
        <div className={styled.auth_form}>
          <div className={styled.auth_form_header}>
            <h1>Create Timesheet</h1>
            <h6>
              Staff code: <span>{timeSheetDetail.staff_id}</span>{' '}
            </h6>
            <h6>
              Staff name: <span>{timeSheetDetail.staff_name}</span>{' '}
            </h6>
            <h6>
              Staff department: <span>{timeSheetDetail.department}</span>
            </h6>
          </div>

          <Form
            className={styled.auth_form_content}
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              className={styled.auth_form_input}
              label="Title"
              name="title"
              rules={[{ required: true, message: 'Please input title' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Time"
              name="time"
              rules={[{ required: true, message: 'Please input Time!' }]}
            >
              <DatePicker onChange={onChangeDate} />
              <TimePicker.RangePicker use12Hours format="h:mm A" />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Description"
              name="description"
              rules={[
                { required: true, message: 'Please input Task Description!' },
              ]}
            >
              <TextArea rows={4} />
            </Form.Item>

            <Upload>
              <Button icon={<UploadOutlined />}>Click to Upload</Button>
            </Upload>

            <Row justify="center">
              <Col>
                <Button
                  className={styled.auth_form_button}
                  size="large"
                  shape="round"
                  type="primary"
                  htmlType="submit"
                >
                  Create
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </Col>
      {/* <Col span={8}></Col> */}
    </Row>
  );
};

export default NewTimeSheet;
