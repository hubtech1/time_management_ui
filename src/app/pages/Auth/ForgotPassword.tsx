import React, { useState } from 'react';
import { Button, Form, Input, Col, Row } from 'antd';
import styled from './AuthPage.module.css';

const ForgotPassword = () => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Row className={styled.auth_page}>
      {/* <Col span={8}></Col> */}
      <Col className={styled.auth_container} span={8}>
        <div className={styled.auth_form}>
          <div className={styled.auth_form_header}>
            <h1>Forgot password!</h1>
            <h3>Let us help you</h3>
          </div>

          <Form
            className={styled.auth_form_content}
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              className={styled.auth_form_input}
              label="Staff code"
              name="staffCode"
              rules={[
                { required: true, message: 'Please input your staff code!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Email"
              name="email"
              rules={[{ required: true, message: 'Please input your email!' }]}
            >
              <Input />
            </Form.Item>

            <Row justify="center">
              <Col>
                <Button
                  className={styled.auth_form_button}
                  size="large"
                  shape="round"
                  type="primary"
                  htmlType="submit"
                >
                  Send mail
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </Col>
      {/* <Col span={8}></Col> */}
    </Row>
  );
};

export default ForgotPassword;
