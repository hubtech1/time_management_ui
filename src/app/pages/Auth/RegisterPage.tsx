import React, { useEffect, useState } from 'react';
import { Button, Form, Input, Col, Row, Select, notification } from 'antd';
import styled from './AuthPage.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getDepartment } from 'store/entitySlice/departmentSlice';
import { createStaff } from 'store/entitySlice/staffSlice';

const RegisterPage = props => {
  const history = useHistory();
  const departmentList = useSelector((state: any) => state.departmentReducer);
  const { register } = useSelector((state: any) => state.staffReducer);

  const dispatch = useDispatch();
  const [form] = Form.useForm();

  console.log('staffReducer', register);

  const { Option } = Select;
  type NotificationType = 'success' | 'info' | 'warning' | 'error';

  const openNotification = (type: NotificationType) => {
    switch (type) {
      case 'warning':
        {
          notification[type]({
            message: 'Notification',
            description: 'Staffcode already exists!!!',
          });
        }
        break;
      case 'success':
        {
          notification[type]({
            message: 'Notification',
            description: 'Rigister success!!!',
          });
        }
        break;
      default:
        return;
    }
  };

  const renderDepartmentList = () => {
    return departmentList.list.map((department: any, index: number) => {
      return (
        <Option key={index} value={department.id}>
          {department.departmentName}
        </Option>
      );
    });
  };

  useEffect(() => {
    dispatch(getDepartment());
  }, []);

  useEffect(() => {
    if (register.staffCodeExist) {
      console.log('warning');
      openNotification('warning');
    } else {
      console.log('success');
      openNotification('success');
    }
  }, [register.staffCodeExist]);

  const onFinish = (values: any) => {
    if (values.password !== values.repeatPassword) {
      alert('Mật Khẩu Không Trùng Nhau!');
    }
    const standartValue = { ...values, role: +values.role };
    delete standartValue.repeatPassword;
    dispatch(createStaff(standartValue));
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const callApi = () => {
    console.log('callApi');
    openNotification('success');
  };

  return (
    <Row className={styled.auth_page}>
      <Col className={styled.auth_container} span={8}>
        <div className={styled.auth_form}>
          <div className={styled.auth_form_header}>
            <h1>Hi!</h1>
            <h3>Create a new account</h3>
          </div>

          <Form
            className={styled.auth_form_content}
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              className={styled.auth_form_input}
              label="Staff code"
              name="staffCode"
              rules={[
                { required: true, message: 'Please input your staff code!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Full name"
              name="fullName"
              rules={[
                { required: true, message: 'Please input your full name!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Name Department"
              name="departmentId"
              rules={[
                { required: true, message: 'Please choose your department!' },
              ]}
            >
              <Select placeholder="Select your department" allowClear>
                {/* <Option value="Zhejiang">Zhejiang</Option>
                    <Option value="Jiangsu">Jiangsu</Option> */}
                {renderDepartmentList()}
              </Select>
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Role"
              name="role"
              rules={[{ required: true, message: 'Please choose your Role!' }]}
            >
              <Select placeholder="Select your department" allowClear>
                <Option value="0">0 - Staff</Option>
                <Option value="1">1 - Manager</Option>
                <Option value="2">2 - Director</Option>
              </Select>
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Email"
              name="email"
              rules={[{ required: true, message: 'Please input your email!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Password"
              name="password"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              label="Repeat Password"
              name="repeatPassword"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Row justify="center">
              <Col>
                <Button
                  className={styled.auth_form_button}
                  size="large"
                  shape="round"
                  type="primary"
                  htmlType="submit"
                >
                  Register
                </Button>

                <Button
                  size="large"
                  shape="round"
                  type="dashed"
                  // htmlType="submit"
                  onClick={callApi}
                >
                  Call API
                </Button>

                <Button onClick={() => openNotification('warning')}>
                  Warning
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </Col>
    </Row>
  );
};

export default RegisterPage;
