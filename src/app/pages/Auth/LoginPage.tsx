import React, { useState } from 'react';
import { Button, Form, Input, Col, Row } from 'antd';
import styled from './AuthPage.module.css';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { login } from 'store/authSlice/authSlice';

const LoginPage = props => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { isAuthenticated, item } = useSelector(
    (state: any) => state.authReducer,
  );

  const navPath = {
    register: 'register',
    loginSuccess: 'mainpage',
    forgotPass: 'forgotpassword',
  };
  const onFinish = (values: any) => {
    dispatch(login(values));
  };

  if (isAuthenticated) {
    // history.push(navPath.loginSuccess);
    props.history.push('/mainpage');
  }

  const goToForgotPass = () => {
    let pathForgotpass = `forgotpassword`;
    history.push(pathForgotpass);
  };

  const goToRegister = () => {
    let pathRegister = `register`;
    history.push(pathRegister);
  };

  return (
    <Row className={styled.auth_page}>
      <Col className={styled.auth_container} span={8}>
        <div className={styled.auth_form}>
          <div className={styled.auth_form_header}>
            <h1>Welcome!</h1>
            <h3>Sign in to continue</h3>
          </div>

          <Form
            className={styled.auth_form_content}
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              className={styled.auth_form_input}
              initialValue="AW150707"
              label="Staff code"
              name="staffCode"
              rules={[
                { required: true, message: 'Please input your staff code!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className={styled.auth_form_input}
              initialValue="tdtrong1507"
              label="Password"
              name="password"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Row justify="center">
              <Col>
                <Button
                  className={styled.auth_form_button}
                  size="large"
                  shape="round"
                  type="primary"
                  htmlType="submit"
                >
                  Login
                </Button>
              </Col>
            </Row>
          </Form>

          <div className={styled.auth_form_footer}>
            <p>{isAuthenticated ? '' : item.mess}</p>
            <h6 onClick={goToForgotPass}>Forgot Password?</h6>
            <h5 onClick={goToRegister}>Create a new account</h5>
          </div>
          {isAuthenticated ? <p>Login success</p> : <p>Not yet</p>}
        </div>
      </Col>
    </Row>
  );
};

export default LoginPage;
