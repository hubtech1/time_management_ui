import { Button } from 'antd';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useDispatch, useSelector } from 'react-redux';
import { login } from 'store/authSlice/authSlice';
import { findAll } from 'store/entitySlice/staffSlice';
import { removeArray, addArray } from 'store/testReducer/testReducer';

export function HomePage() {
  const modifyAray = useSelector((state: any) => state.testReducer);
  const isAuthenticated = useSelector(
    (state: any) => state.authReducer.isAuthenticated,
  );

  const dispatch = useDispatch();
  const [inputState, setInputState] = React.useState('hello');
  const [inputStateLogin, setInputStateLogin] = React.useState({
    values: {
      staffCode: 'AW150707',
      password: 'tdtrong1507',
    },
  });

  const renderArrayList = () => {
    return modifyAray.map((item: any, index: number) => {
      return (
        <div
          style={{ height: `30px`, overflow: 'hidden' }}
          className="d-flex align-items-center my-1 pr-5 col-sm-2"
          key={index}
        >
          <button
            onClick={() => {
              dispatch(removeArray(index));
            }}
            className="btn btn-danger btn-sm mr-2"
          >
            -
          </button>
          <span>{item}</span>
        </div>
      );
    });
  };

  const handleChangeInput = e => {
    let { name, value } = e.target;
    let newValues = { ...inputStateLogin.values, [name]: value };

    setInputStateLogin({
      ...inputStateLogin,
      values: newValues,
    });
  };

  return (
    <>
      <Helmet>
        <title>HomePage</title>
        <meta name="description" content="A Boilerplate application homepage" />
      </Helmet>

      {/* test testReducer */}
      <div className="container">
        <div className="mb-5">
          <input
            style={{ verticalAlign: 'middle' }}
            className=""
            aria-label="Set increment amount"
            value={inputState}
            onChange={e => setInputState(e.target.value)}
          />
          <button
            onClick={() => {
              dispatch(addArray(inputState));
              setInputState('');
            }}
            className="btn btn-success mx-3"
          >
            Add array
          </button>
        </div>
        <div className="row">{renderArrayList()}</div>
      </div>

      {/* test authReducer */}
      <div className="container mt-5">
        <form>
          <div className="form-group">
            <label>Staff Code</label>
            <input
              type="text"
              name="staffCode"
              value={inputStateLogin.values.staffCode}
              onChange={handleChangeInput}
              className="form-control"
              id="inputEmail1"
              placeholder="Staff Code"
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="text"
              name="password"
              value={inputStateLogin.values.password}
              onChange={handleChangeInput}
              className="form-control"
              id="inputPassword1"
              placeholder="Password"
            />
          </div>

          {/* Test login API */}
          <button
            onClick={() => {
              dispatch(login(inputStateLogin.values));
            }}
            type="button"
            className="btn btn-primary"
          >
            Login
          </button>

          {/* Test get list API */}

          <button
            type="button"
            onClick={() => {
              dispatch(findAll());
            }}
            className="btn btn-success ml-3"
          >
            Test call api find all list
          </button>
        </form>

        {isAuthenticated ? <p>Login success</p> : <p>Not yet</p>}
      </div>

      {/* Main page */}
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          // height: '100vh',
        }}
      >
        <Button href="./login" size="large" type="primary">
          Go to login
        </Button>

        <Button href="./mainpage" size="large" danger type="primary">
          Go to main page
        </Button>
      </div>
    </>
  );
}
