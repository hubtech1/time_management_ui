import CalendarComponent from 'app/components/Calendar/CalendarComponent';
import NavbarAvatar from 'app/components/NavbarAvatar/NavbarAvatar';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import ContentHeader from './subComponents/ContentHeader';
import ManagementBodyContent from './subComponents/ManagementBodyContent';
import SideSliderName from './subComponents/SideSliderName';
import './TimeManagement.css';
import { useDispatch } from 'react-redux';
import { findOne } from 'store/entitySlice/staffSlice';

const hostInfo = {
  id: 'aw1507',
  name: 'Allen Walker',
  role: 'Director',
};

const TimeManagement = () => {
  const localStorageInfo: any = localStorage.getItem('staff_info');
  const { staffCode } = JSON.parse(localStorageInfo);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(findOne(staffCode));
  }, []);

  return (
    <div className="management_container">
      <div className="management_header">
        <NavbarAvatar />
      </div>
      <div className="management_body">
        <div className="management_body_slider">
          <CalendarComponent />
          <div className="management_body_slider_name">
            <SideSliderName hostInfo={hostInfo} />
          </div>
        </div>
        <div className="management_body_content">
          <ContentHeader />
          <ManagementBodyContent />
        </div>
      </div>
    </div>
  );
};

export default TimeManagement;
