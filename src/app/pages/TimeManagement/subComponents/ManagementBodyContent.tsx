import type { BadgeProps } from 'antd';

import { Badge, Calendar } from 'antd';
import type { CalendarMode } from 'antd/lib/calendar/generateCalendar';
import type { Moment } from 'moment';
import React from 'react';
import './ManagementBodyContent.css';

const ManagementBodyContent = props => {
  const getListData = (value: Moment) => {
    let listData;
    console.log('calendar', value.date());

    switch (value.date()) {
      // case 8:
      //   listData = [
      //     { type: 'warning', content: 'This is warning event.' },
      //     { type: 'success', content: 'This is usual event.' },
      //   ];
      //   break;
      // case 10:
      //   listData = [
      //     { type: 'warning', content: 'This is warning event.' },
      //     { type: 'success', content: 'This is usual event.' },
      //     { type: 'error', content: 'This is error event.' },
      //   ];
      //   break;
      // case 15:
      //   listData = [
      //     { type: 'warning', content: 'This is warning event' },
      //     { type: 'success', content: 'This is very long usual event。。....' },
      //     { type: 'error', content: 'This is error event 1.' },
      //     { type: 'error', content: 'This is error event 2.' },
      //     { type: 'error', content: 'This is error event 3.' },
      //     { type: 'error', content: 'This is error event 4.' },
      //   ];
      // break;
      default:
    }
    return listData || [];
  };

  const dateCellRender = (value: Moment) => {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map(item => (
          <li key={item.content}>
            <Badge
              status={item.type as BadgeProps['status']}
              text={item.content}
            />
          </li>
        ))}
      </ul>
    );
  };

  const onPanelChange = (value: Moment, mode: CalendarMode) => {
    console.log(value.format('YYYY-MM-DD'), mode);
  };

  return (
    <div className="management_body_content_calendar">
      <Calendar
        dateCellRender={dateCellRender}
        fullscreen={true}
        onPanelChange={onPanelChange}
      />
    </div>
  );
};

export default ManagementBodyContent;
