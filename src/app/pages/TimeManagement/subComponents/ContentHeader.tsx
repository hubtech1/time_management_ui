import { DownOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu, MenuProps, message, Space } from 'antd';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './ContentHeader.css';

const ContentHeader = () => {
  const [timeState, setTimeState] = useState({
    label: '',
  });

  const handleMenuClick: MenuProps['onClick'] = e => {
    // message.info('Click on menu item.');
    // console.log('click', e);
    // console.log("menu", menu.props.items);

    // let newArr = [...menu.props.items]
    // console.log("newArr" , newArr);
    let filterArr = menu.props.items.filter(item => item.key == e.key);
    setTimeState({
      label: filterArr[0].label,
    });
  };

  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[
        {
          label: 'Week',
          key: '1',
        },
        {
          label: 'Month',
          key: '2',
        },
      ]}
    />
  );

  const history = useHistory();

  const addTimeSheet = () => {
    let path = `timesheet`;
    history.push(path);
  };

  const modifyTimeSheet = () => {
    let path = `oldtimesheet`;
    history.push(path);
  };

  return (
    <div className="management_body_content_header">
      <div className="content_header_time">
        <Dropdown overlay={menu}>
          <Button>
            <Space>
              {timeState.label === '' ? 'Choice' : timeState.label}
              <DownOutlined />
            </Space>
          </Button>
        </Dropdown>
      </div>

      <div className="content_header_action">
        <Button onClick={addTimeSheet} type="primary">
          Add time
        </Button>
      </div>

      <div className="content_header_action">
        <Button onClick={modifyTimeSheet} type="primary">
          Modify time
        </Button>
      </div>
    </div>
  );
};

export default ContentHeader;
