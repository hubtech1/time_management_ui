import { UserAddOutlined } from '@ant-design/icons';
import { Button, Form, Input } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { findColleague } from 'store/entitySlice/staffSlice';
import './SideSliderName.css';

const SideSliderName = (props: any) => {
  const [colleagueList, setColleagueList] = useState([
    // { id: 'KK08', fullName: 'Kaka', role: 'staff' },
    // { id: 'CR07', fullName: 'Cristiano Ronaldo', role: 'staff' },
    // { id: 'R10', fullName: 'Rooney', role: 'staff' },
    // { id: 'M10', fullName: 'Messi', role: 'staff' },
    // { id: 'R4', fullName: 'Ramos', role: 'staff' },
  ]);
  const { staff, listColleague } = useSelector(
    (state: any) => state.staffReducer,
  );
  const dispatch = useDispatch();

  const renderColleagueList = () => {
    return colleagueList.map((colleague: any, index) => {
      return (
        <li key={index} className="side_slider_list_item">
          <input name="staffChoice" type="radio" />
          {colleague ? colleague.fullName : 'Loading'}
        </li>
      );
    });
  };

  const onFinish = (values: any) => {
    dispatch(findColleague(values.staffCode));
  };

  useEffect(() => {
    setColleagueList(listColleague);
  }, [listColleague]);

  console.log('listColleague reducer', listColleague);
  console.log('listColleague component', colleagueList);

  return (
    <ul className="side_slider_list">
      <Form
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Staff Code"
          name="staffCode"
          rules={[{ required: true, message: 'Please input Staff Code!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Form.Item>
      </Form>

      <div className="side_slider_list_heading">
        <h3>My calendar</h3>
      </div>
      <li className="side_slider_list_item">
        <input name="staffChoice" type="radio" />
        {staff ? staff.fullName : ''}
      </li>
      <div className="side_slider_list_heading">
        <h3>My colleagues</h3>
        <UserAddOutlined
          style={{ fontSize: '20px', color: '#ED2553', paddingRight: '0px' }}
        />
      </div>
      {renderColleagueList()}
    </ul>
  );
};

export default SideSliderName;
