import entityService from './services/identity';

export const DepartmentApi = {
  find: async () => {
    try {
      const response = await entityService.get('departments');

      return response;
    } catch (error) {
      return error;
    }
  },
};
