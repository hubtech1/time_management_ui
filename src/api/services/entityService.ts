import axios from 'axios';

const { CancelToken } = axios;
export const sourceCancel = CancelToken.source();

const baseURL = 'http://localhost:4004/api/v1/';

const localStorageInfo: any = localStorage.getItem('staff_info');
if (localStorageInfo) {
  var { access_token } = JSON.parse(localStorageInfo);
}

const entityService = axios.create({
  baseURL: baseURL,
  headers: {
    accept: 'application/json',
    token: access_token,
    'Content-Type': 'application/json',
  },
});

export default entityService;
