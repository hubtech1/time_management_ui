import axios from 'axios';

const { CancelToken } = axios;
export const sourceCancel = CancelToken.source();

const baseURL = 'http://localhost:4004/api/v1/';

const identityService = axios.create({
  baseURL: baseURL,
  // withCredentials: true,
  // cancelToken: sourceCancel.token,
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

export default identityService;
