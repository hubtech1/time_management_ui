import entityService from './services/entityService';

export const StaffApi = {
  findAll: async () => {
    try {
      const response = await entityService.get('staffs');
      // console.log("entityService",entityService.defaults.headers.common.Authorization);
      // console.log("StaffApi", response);

      return response;
    } catch (error) {
      return error;
    }
  },

  findOne: async (id: string) => {
    try {
      const response = await entityService.get(`staffs/${id}`);

      return response;
    } catch (error) {
      return error;
    }
  },

  findColleague: async (id: string) => {
    try {
      const response = await entityService.get(`staffs/${id}`);

      return response;
    } catch (error) {
      return error;
    }
  },

  create: async (body: any) => {
    try {
      const response = await entityService.post(`register`, body);
      console.log('StaffApi ', response.data);

      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
