import identityService from './services/identity';

export const AuthApi = {
  login: async (credential: any) => {
    try {
      const response = await identityService.post('login', credential);
      // console.log("credentials call api" , response);
      return response;
    } catch (error) {
      return error;
    }
  },

  logout: async () => {
    try {
      const response = await identityService.post(`logout`);
      return response;
    } catch (error) {
      // throw error;
    }
  },
};
